/*
Basic configuration to run your cucumber
feature files and step definitions with protractor.
**/
let SpecReporter = require('jasmine-spec-reporter').SpecReporter;
let jasmine
let getEnv
var HtmlReporter = require('protractor-html-screenshot-reporter');

exports.config = {
  capabilities: {
    //ignoreSynchronization: true,
    browserName: 'chrome',
    shardTestFiles: true
  },

  allScriptsTimeout: 30000,
  // baseUrl: 'https://angularjs.org',

  framework: 'custom',  // set to "custom" instead of cucumber.
  frameworkPath: require.resolve('protractor-cucumber-framework'),  // path relative to the current config file

  specs: [
    './e2e/features/clubUser/likeComment.feature'
    // './e2e/features/clubUser/image.feature'
    // './e2e/features/clubUser/addBankAccount.feature'
    // './e2e/features/clubUser/eventPost.feature'
    // './e2e/features/clubUser/signUp.feature'

    // './e2e/features/endUser/changePassword.feature'
    // './e2e/features/endUser/reportEvent.feature'

    // './e2e/features/endUser/likeComment.feature'
    // './e2e/features/endUser/ticketBooking.feature'
    // './e2e/features/endUser/loginwithgoogle.feature'
    // './e2e/features/endUser/login.feature'
  // './e2e/features/endUser/signUp.feature'  // Specs here are the cucumber feature files
  ],

  restartBrowserBetweenTests: true,
  // cucumber command line options
  cucumberOpts: {
    require: [
      './e2e/specs/clubUser/likecomment.spec.js'
      // "./e2e/specs/clubUser/imageUpload.spec.js"
      // "./e2e/specs/clubUser/addBankAccount.spec.js"
      // "./e2e/specs/clubUser/eventPost.spec.js"
      // './e2e/specs/clubUser/signUp.spec.js' 
      // './e2e/specs/endUser/changePassword.spec.js'
      // './e2e/specs/endUser/reportEvent.spec.js'
      // './e2e/specs/endUser/likeComment.spec.js'
      // './e2e/specs/endUser/ticketBooking.spec.js'
      // './e2e/specs/endUser/loginwithgoogle.spec.js'
      // './e2e/specs/endUser/login.spec.js'
      // './e2e/specs/endUser/signUp.spec.js'
      
    ],  // require step definition files before executing features
    tags: [],                      // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    strict: true,                  // <boolean> fail if there are any undefined or pending steps
    'dry-run': false,              // <boolean> invoke formatters without executing steps
    compiler: [],                   // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
  },
  
  onPrepare: function () {
    browser.manage().window().maximize(); // maximize the browser before executing the feature files
    const { Given, Then, When, Before } = require('cucumber');
    global.Given = Given;
    global.When = When;
    global.Then = Then;
    global.Before = Before;
  },
  // onPrepare: function() {
  //   // Add a screenshot reporter and store screenshots to `/tmp/screnshots`:
  //   jasmine.getEnv().addReporter(new HtmlReporter({
  //      baseDirectory: '/tmp/screenshots'
  //   }));
 

};
