module.exports={
    init:()=>{
        LoginIcon = element(by.xpath("//*[@id='modal--open']/app-root/div/app-navbar/mdb-navbar/nav/div/div[2]/links/ul/li[1]/a"));
        SignUpLink = element(by.xpath("//a[contains(@class,'signUp')]"));
        SignUpAsClubUser = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-end-user-signup/div/div[2]/p[2]/span"));
        ClubName = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-club-user-signup/div/div/div/div/div/div/div/form/div[1]/input"));
        PhoneNumber = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-club-user-signup/div/div/div/div/div/div/div/form/div[2]/input"));
        EmailId = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-club-user-signup/div/div/div/div/div/div/div/form/div[3]/input"));
        Introduction = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-club-user-signup/div/div/div/div/div/div/div/form/div[4]/input"));
        UserName = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-club-user-signup/div/div/div/div/div/div/div/form/div[6]/input"))
        Password = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-club-user-signup/div/div/div/div/div/div/div/form/div[7]/input"));
        NextButton = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-club-user-signup/div/div/div/div/div/div/div/form/div[8]/button"))
        ClubLocation = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-club-user-signup/div/div/div/div/div/div/div/form[1]/label[2]"));
        LocationName = element(by.xpath("//*[@id='mat-dialog-0']/app-locations/div/div/input"));
        Suggestion = browser.actions().sendKeys(protractor.Key.ENTER).perform();
        OutsideClick = browser.actions().sendKeys(protractor.Key.TAB).perform();
        SaveChangesButton = element(by.xpath("//*[@id='mat-dialog-0']/app-locations/div/button[2]"));
        State = element(by.xpath("//*[@id='stateName']"));
        StateSuggestion = element(by.xpath("//*[@id='ngb-typeahead-0-0']"));
        City = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-club-user-signup/div/div/div/div/div/div/div/form[1]/div[2]/input"));
        // CitySuggestion =element(by.xpath("//*[@id='ngb-typeahead-1-0']"));
        Street = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-club-user-signup/div/div/div/div/div/div/div/form[1]/div[3]/input"));
        Zipcode = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-club-user-signup/div/div/div/div/div/div/div/form[1]/div[4]/input"));
        FromHour = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-club-user-signup/div/div/div/div/div/div/div/form[2]/div/div/div[1]/div[1]/div/div/ngb-timepicker/fieldset/div/div[1]/input"));
        FromMinute = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-club-user-signup/div/div/div/div/div/div/div/form[2]/div/div/div[1]/div[1]/div/div/ngb-timepicker/fieldset/div/div[3]/input"));
        ToHour = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-club-user-signup/div/div/div/div/div/div/div/form[2]/div/div/div[1]/div[2]/div/div/ngb-timepicker/fieldset/div/div[1]/input"));
        ToMinute = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-club-user-signup/div/div/div/div/div/div/div/form[2]/div/div/div[1]/div[2]/div/div/ngb-timepicker/fieldset/div/div[3]/input"));
        ClickDay = element(by.xpath("//*[@id='mat-select-1']/div/div[2]/div"));
        SelectDay = element(by.xpath("//*[@id='mat-option-5']/mat-pseudo-checkbox"));
        SignUpButton = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-club-user-signup/div/div/div/div/div/div/div/div[2]/button"));
        
        
          
    },

    get(string){
        return browser.get('https://theopallabs.com');
      },
    setIcon:() => {
        return LoginIcon.click();
      },
    setSignUpLink:()=>{
          return SignUpLink.click();
      },
      setclubUserSignUp:()=>{
          return SignUpAsClubUser.click();
      },
      setClubName:()=>{
         return ClubName.sendKeys("abdg");
      },
      setPhoneNumber:()=>{
          return PhoneNumber.sendKeys("9965884258");
      },
      setEmailId:()=>{
          return EmailId.sendKeys("club@gmail.com");
      },
      setIntroduction:()=>{
          return Introduction.sendKeys("club")
      },
      setUsername:()=>{
          return UserName.sendKeys("club03");
      },
      setPassword:()=>{
          return Password.sendKeys("123456");
      },
      setNextButton:()=>{
          return NextButton.click();
      },
      setClubLocation:()=>{
          return ClubLocation.click();
      },
      setLocationName:()=>{
        return LocationName.sendKeys("pune");
      },
      setSuggestion :()=>{
        return suggestionFocus.click();
      },
      setSaveChangeButton:()=>{
          return SaveChangesButton.click();
      },
      setState:()=>{
        return State.sendKeys("Maharashtra");
      },
      setStateSuggestion:()=>{
         return StateSuggestion.click();
      },
      setCity:()=>{
        return City.sendKeys("Pune");
      },
      setCitySuggestion:()=>{
         return CitySuggestion.click();
      },
      setStreet:()=>{
        return Street.sendKeys("jm road");
      },
      setZipCode:()=>{
        return Zipcode.sendKeys("152004");
      },
      setFromHour:()=>{
          return FromHour.sendKeys("08");
      },
      setFromMinutes:()=>{
          return FromMinute.sendKeys("10");
      },
      setToHour:()=>{
        return ToHour.sendKeys("10");
    },
    setToMinutes:()=>{
        return ToMinute.sendKeys("30");
    },
    setClickDay:()=>{
        return ClickDay.click();
    },
    setSelectDay:()=>{
        return SelectDay.click();
    },
    setOutSideClick:()=>{
         return OutsideClick;
    },
    setRequestButton:()=>{
       return SignUpButton.click();
    },

}