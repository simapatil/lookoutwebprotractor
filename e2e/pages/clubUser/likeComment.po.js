module.exports={
 init:()=>{
    Icon = element(by.xpath("//*[@id='modal--open']/app-root/div/app-navbar/mdb-navbar/nav/div/div[2]/links/ul/li[1]/a"));
    userName = element(by.xpath("//div[@class='form-group']//input[@type='text']"));
    password = element(by.xpath("//input[@type='password']"));
    LoginButton = element(by.className("btn btn-login"));
    LikeButton = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-club-dashboard/div/div/div[2]/app-create-post/app-time-line/div/div/div[1]/app-event-card/div/div/div[2]/div/div/div[3]/div/span[1]/span[1]/img"));
    CommentButton = element(by.xpath("//*[@id='dropdownConfig']"));
    CommentTextBox = element(by.xpath("//*[@id='wrapper']/div/div[2]/input")); 
    SendButton = element(by.xpath("//*[@id='wrapper']/div/div[2]/button"));

},

  get: (string) => {
    return browser.get('https://theopallabs.com');
  },
  setIcon:() => {
    return Icon.click();
  },
  setUserName: () => {
    return userName.sendKeys("club11");
    // expect(userName.sendKeys("sima")).toEqual(null);
  },
  setPassword:() => {
    return password.sendKeys("123456");
  },
  setLoginBtn:()=> {
    return LoginButton.click();
  },
  setLikeButton:()=>{
      return LikeButton.click();
  },
  setCommentClick:()=>{
      return CommentButton.click();
  },
  setCommentText:()=>{
      return CommentTextBox.sendKeys('abcddddd');
  },
  setSendButton:()=>{
      return SendButton.click();
  }
}
