module.exports = {
    init:()=> {
        Icon = element(by.xpath("//*[@id='modal--open']/app-root/div/app-navbar/mdb-navbar/nav/div/div[2]/links/ul/li[1]/a"));
        userName = element(by.xpath("//div[@class='form-group']//input[@type='text']"));
        password = element(by.xpath("//input[@type='password']"));
        LoginButton = element(by.className("btn btn-login"));
        UserIcon = element(by.xpath("//*[@id='modal--open']/app-root/div/app-navbar/mdb-navbar/nav/div/div[2]/links/ul[2]/div/a"));
        Settings = element(by.xpath("//*[@id='modal--open']/app-root/div/app-navbar/mdb-navbar/nav/div/div[2]/links/ul[2]/div/div/a[2]"));
        BankSetting = element(by.xpath("//*[@id='mat-expansion-panel-header-1']/span[1]/mat-panel-title"));
        AddBankAccount = element(by.xpath("//*[@id='cdk-accordion-child-1']/div/button[1]/span"));
        IFSC = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-club-dashboard/div/div/div[2]/app-add-bank/div/div/div/form/div[1]/input"));
        AccountNo = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-club-dashboard/div/div/div[2]/app-add-bank/div/div/div/form/div[2]/input"));
        ConfirmAccNo = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-club-dashboard/div/div/div[2]/app-add-bank/div/div/div/form/div[3]/input"));
        AccHolderName = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-club-dashboard/div/div/div[2]/app-add-bank/div/div/div/form/div[4]/input"));
        AccountType = element(by.xpath("//*[@id='mat-select-0']/div/div[1]"));
        CurrentAcc = element(by.xpath("//*[@id='mat-option-1']/span"));
        NickName = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-club-dashboard/div/div/div[2]/app-add-bank/div/div/div/form/div[6]/input"));
        AddAccButton = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-club-dashboard/div/div/div[2]/app-add-bank/div/div/div/form/button"));
    },
 
      get: (string) => {
        return browser.get('https://theopallabs.com');
      },
      setIcon:() => {
        return Icon.click();
      },
      setUserName: () => {
        return userName.sendKeys("club11");
        // expect(userName.sendKeys("sima")).toEqual(null);
      },
      setPassword:() => {
        return password.sendKeys("123456");
      },
      setLoginBtn:()=> {
        return LoginButton.click();
      },
      setUserIcon:()=> {
          return UserIcon.click();
      },
      setSetting:()=>{
          return Settings.click();
      },
      setBankSettings:()=>{
          return BankSetting.click();
      },
      setAddBankAcc:()=>{
          return AddBankAccount.click();
      },
      setIFSC:()=>{
        return IFSC.sendKeys("HDFC0000149");
      },
      setAccountNo:()=>{
        return AccountNo.sendKeys("9876541230");
      },
      setConfirmAccNo:()=>{
        return ConfirmAccNo.sendKeys("9876541230");
      },
      setAccHolderName:()=>{
        return AccHolderName.sendKeys("Demoo")
      },
      SetAccType:()=>{
        return AccountType.click();
      },
      setCurrrenAcc:()=>{
        return CurrentAcc.click();
      },
      setNickName :()=>{
        return NickName.sendKeys("Test");
      },
      setAddAccount:()=>{
        return AddAccButton.click();
      }
}