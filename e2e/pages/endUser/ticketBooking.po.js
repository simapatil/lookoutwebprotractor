var EventCard;
var EnterName;
var LoginIcon;
var userName;
var password;
var LoginButton;
var SelectSuggestion;
var AddButton;
var Bookbutton;
var CheckButton;
var Bookbtn;

module.exports = {

    init:() => {

        LoginIcon = element(by.xpath("//*[@id='modal--open']/app-root/div/app-navbar/mdb-navbar/nav/div/div[2]/links/ul/li[1]/a"));
        userName = element(by.xpath("//div[@class='form-group']//input[@type='text']"));
        password = element(by.xpath("//input[@type='password']"));
        LoginButton = element(by.className("btn btn-login"));
        EventCard = element(by.xpath("//span[contains(text(),'Dj night with DjDJDJDJDJDJDJD')]"));
        EnterName=element(by.xpath("//input[@placeholder='Name']"));
        SelectSuggestion=element(by.id("ngb-typeahead-0-0"));
        AddButton = element(by.xpath("//*[@id='bookEvent']/app-book-event/section/form/div/div/div[3]/button/i"));
        Bookbutton = element(by.xpath("//*[@id='bookEvent']/app-book-event/section/button"));
        CheckButton = element(by.xpath("//*[@id='mat-checkbox-1']/label/div"));
        Bookbtn = element(by.xpath("//*[@id='bookEvent']/app-book-event/div/div/div/div[3]"))
    },
    get: (string) => {
        return browser.get('https://theopallabs.com');
      },
      setIcon:() => {
        return LoginIcon.click();
      },
      setUserName: () => {
        return userName.sendKeys("aditya");
        // expect(userName.sendKeys("sima")).toEqual(null);
    
      },

      setPassword:() => {
        return password.sendKeys("123456");
      },

      setLoginBtn:()=> {
        return LoginButton.click();
      },

      setEventCard:() => {
        return EventCard.click();
      },

      setFriendName:() => {
          return EnterName.sendKeys('aditya');
      },

      getSelectSuggestion:()=> {
        return SelectSuggestion.click();
      },

      setAddButton:() => {
        return AddButton.click();
      },

      setBookButton:()=> {
        return Bookbutton.click();
      },

      setCheckButton:()=> {
        return CheckButton.click();
      },

      setBookBtn:()=> {
        return Bookbtn.click();
      }

}