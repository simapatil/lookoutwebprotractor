module.exports ={
    init(){
        LoginIcon = element(by.xpath("//*[@id='modal--open']/app-root/div/app-navbar/mdb-navbar/nav/div/div[2]/links/ul/li[1]/a"));
        userName = element(by.xpath("//div[@class='form-group']//input[@type='text']"));
        password = element(by.xpath("//input[@type='password']"));
        LoginButton = element(by.className("btn btn-login"));
        UserIcon = element(by.xpath("//*[@id='modal--open']/app-root/div/app-navbar/mdb-navbar/nav/div/div[2]/links/ul[2]/div/a"));
        SettingBtn = element(by.xpath("//*[@id='modal--open']/app-root/div/app-navbar/mdb-navbar/nav/div/div[2]/links/ul[2]/div/div/a[2]"));
        GeneralSetting = element(by.xpath("//*[@id='mat-expansion-panel-header-0']/span[1]/mat-panel-title"));
        changePassword = element(by.xpath("//*[@id='cdk-accordion-child-0']/div/button[1]"));
        OldPassword = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-club-dashboard/div/div/div[2]/app-change-password/div/div/form/div/div/div[1]/input"));
        NewPassword = element(by.xpath("//*[@id='InputEmailMobile']"));
        SubmitButton = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-club-dashboard/div/div/div[2]/app-change-password/div/div/form/div/div/button"));
    },

      get(string){
        return browser.get('https://theopallabs.com');
    },
      setIcon:() => {
        return LoginIcon.click();
      },
      setUserName: () => {
        return userName.sendKeys("aditya");    
      },
      setPassword:() => {
        return password.sendKeys("1234567");
      },
      setLoginBtn:()=> {
        return LoginButton.click();
      },
      setUserIcon:()=>{
          return UserIcon.click();
      },
      getSetting:()=>{
          return SettingBtn.click();
      },
      setGeneralSetting:()=>{
          return GeneralSetting.click();
      },
      setChangePassword:()=>{
          return changePassword.click();
      },
      setOldPassword:()=>{
            return OldPassword.sendKeys('1234567');
      },
      setNewPassword:()=>{
          return NewPassword.sendKeys('123456')
      },
      setSubmitButton:()=>{
          return SubmitButton.click();
      },

}