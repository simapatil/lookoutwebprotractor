module.exports ={

    init:()=>{
    LoginIcon = element(by.xpath("//*[@id='modal--open']/app-root/div/app-navbar/mdb-navbar/nav/div/div[2]/links/ul/li[1]/a"));
    userName = element(by.xpath("//div[@class='form-group']//input[@type='text']"));
    password = element(by.xpath("//input[@type='password']"));
    LoginButton = element(by.className("btn btn-login"));
    optionMenu = element(by.xpath("//*[@id='menuBtn']"));
    reportBtn = element(by.xpath("//*[@id='cdk-overlay-0']/div/div/button/span"));
    selectOption = element(by.xpath("//*[@id='mat-checkbox-6']/label/div"));
    sendReportBtn = element(by.xpath("//*[@id='mat-dialog-0']/app-report-abuse/div/div/div/button"));
   
    },
    get(string){
        return browser.get('https://theopallabs.com');
    },
    setIcon:() => {
        return LoginIcon.click();
      },
      setUserName: () => {
        return userName.sendKeys("aditya");    
      },
      setPassword:() => {
        return password.sendKeys("123456");
      },
      setLoginBtn:()=> {
        return LoginButton.click();
      },
      setOptionMenu:()=>{
          return optionMenu.click();
      },
      getReportBtn:()=>{
          return reportBtn.click();
      },
      setSelectOption:()=>{
          return selectOption.click();

      },
      getsendReportBtn:()=>{
        return sendReportBtn.click();
      },


}