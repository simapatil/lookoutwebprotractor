

let LoginIcon
let Search

module.exports = {
  init:()=> {
    LoginIcon = element(by.xpath("//*[@id='modal--open']/app-root/div/app-navbar/mdb-navbar/nav/div/div[2]/links/ul/li[1]/a"));
    Search = element(by.xpath("//input[@placeholder='Search club,event,profile']"));
  },

  get:(string) => {
    return browser.get('https://theopallabs.com');
  },
  setIcon: (icon) => {
    return LoginIcon.click();
  },
   getSearch(search){
    return Search.click();
  },

}

  