
var LoginIcon
var userName
var password
var LoginButton
var Like
var comment
var commentText
module.exports = {

    init:() =>{
        LoginIcon = element(by.xpath("//*[@id='modal--open']/app-root/div/app-navbar/mdb-navbar/nav/div/div[2]/links/ul/li[1]/a"));
        userName = element(by.xpath("//div[@class='form-group']//input[@type='text']"));
        password = element(by.xpath("//input[@type='password']"));
        LoginButton = element(by.className("btn btn-login"));
        Like = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-end-user-home-page/div/div/div[2]/app-end-user-time-line/div/div/div/div[1]/div/span[1]/app-event-card/div/div/div[2]/div/div/div[3]/div/span[1]/span[1]/img"));
        comment = element(by.xpath("//*[@id='dropdownConfig']"));
        commentText = element(by.xpath("//*[@id='wrapper']/div/div[2]/input"));
        sendButton = element(by.xpath("//*[@id='wrapper']/div/div[2]/button"));


    },
    get: (string) => {
        return browser.get('https://theopallabs.com');
      },
      setIcon:() => {
        return LoginIcon.click();
      },
      setUserName: () => {
        return userName.sendKeys("aditya");
        // expect(userName.sendKeys("sima")).toEqual(null);
    
      },
      setPassword:() => {
        return password.sendKeys("123456");
      },
      setLoginBtn:()=> {
        return LoginButton.click();
      },
      getLike:()=>{
        return Like.click();
      },
      setCommentIcon:()=>{
          return comment.click();
      },

      getCommentTExt:()=>{
          return commentText.sendKeys("123");
      },

      setSendButton:()=>{
          return sendButton.click();
      },

    }