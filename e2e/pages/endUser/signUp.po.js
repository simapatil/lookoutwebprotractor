var loginIcon
var signUpLink
var emailId
var name
var introdution
var city
var gender
var male
var dateOfBirth
var nextButton
var userName
var passWord
var mobileNumber
var submitButton
var date
var picker
var selectDate
var datep


module.exports = {

  init: () => {

    loginIcon = element(by.xpath("//*[@id='modal--open']/app-root/div/app-navbar/mdb-navbar/nav/div/div[2]/links/ul/li[1]/a"));
    signUpLink = element(by.xpath("//a[contains(@class,'signUp')]"));
    emailId = element(by.xpath(".//input[@type='email']"));
    name = element(by.xpath("//input[@placeholder='Name']"));
    introdution = element(by.xpath("//input[@placeholder='Introduction']"));
    city = element(by.xpath("//input[@placeholder='City']"));
    gender = element(by.xpath("//div[@class='mat-select-value']"));
    male = element(by.xpath("//span[@class='mat-option-text'][contains(text(),'Male')]"))
    dateOfBirth = element(by.xpath("//input[@id='mat-input-0']"));
    nextButton = element(by.xpath("//button[contains(text(),'Next')]"));
    userName = element(by.xpath("//*[@id='modal--open']/app-root/div/div/app-sign-up/div/app-end-user-signup/div/div[1]/div/app-end-user-account-info/div/div/div/div/div/div/div/form/div[1]/input"));
    passWord = element(by.xpath("//input[@placeholder='Password']"));
    mobileNumber = element(by.xpath("//input[@placeholder='Mobile Number']"));
    submitButton = element(by.xpath("//button[contains(text(),'Sign Up')]"));
    picker = element(by.xpath("//*[@id='mat-datepicker-0']/div[2]/mat-month-view/table/tbody/tr[3]/td[4]/div"));
  },

  get: (string) => {
    return browser.get('https://theopallabs.com');
  },
  setIcon: () => {
    return loginIcon.click();
  },

  getSignUpLink: () => {
    return signUpLink.click();

  },

  getEmailId: () => {
    return emailId.sendKeys("melayer.chetan@gmail.com");
  },

  getName: () => {
    return name.sendKeys("chetan");
  },

  getIntroduction: () => {
    return introdution.sendKeys("user");

  },

  getCity: () => {
    return city.sendKeys("Pune");
  },

  getGender: () => {
    return gender.click();
  },

  setGender: () => {
    // browser.actions().mouseMove(nextButton).perform();
    return male.click();
  },
  getDOB: () => {
    // window.scrollTo(0, document.body.scrollHeight)
    return dateOfBirth.click();

  },

  setdate: () => {
    return picker.click();
  },

  getNextBtn: () => {
    return nextButton.click();
  },

  setUserName: () => {
    return userName.sendKeys("chetan")
  },

  setPassWord: () => {
    return passWord.sendKeys("123456")
  },

  setMobileNumber: () => {
    return mobileNumber.sendKeys("8412004187")
  },

  GetSubmitButton: () => {
    return submitButton.click();
  },

  }
