let SignUpPage =  require('../../pages/endUser/signUp.po')
let chai = require('chai');
let chaiAsPromised =  require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;


Before(function(){
SignUpPage.init();

})
  
         Given('The user go to {string}', function (string) {
         SignUpPage.get(string)
         });

        When('User click on user icon', function () {
             SignUpPage.setIcon()
         });

        Then('User click on SignUp link', function () {
           SignUpPage.getSignUpLink()
        });
 
        Then('User should enter emailId', function () {
           SignUpPage.getEmailId()
         });

        Then('User should enter Name', function () {
           SignUpPage.getName()
         });

        Then('user should enter Introduction', function () {
           SignUpPage.getIntroduction() 
         });

        Then('User should enter City', function () {
           SignUpPage.getCity() 
         });

        Then('User should select Gender', function () {
          SignUpPage.getGender()
         });

        Then('User should select genderOption', function () {
           SignUpPage.setGender()
        });

        Then('User should enter DOB',{timeout:60*90}, function () {
           SignUpPage.getDOB()
           
         });
         
        Then('user should select the date', function () {
          SignUpPage.setdate()
          });
        
           

        Then('User should click on next button', function () {
          SignUpPage.getNextBtn();
        });

 
        Then('User Should enter UserName', function () {
         SignUpPage.setUserName();
        });

        Then('User should enter Password', function () {
          SignUpPage.setPassWord()
            
           });

        Then('user should enter MobileNumber', function () {
          SignUpPage.setMobileNumber()
            
           });
  
        Then('User should click on Submit Button', function () {
          SignUpPage.GetSubmitButton()
            
           });

        
        