let ticketPage = require('../../pages/endUser/ticketBooking.po.js');

let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;

Before(function () {
    ticketPage.init();
});

Given('The user go to {string}', function (string) {
    ticketPage.get(string)
});


When('User click on user icon', function () {
    ticketPage.setIcon()

});

Then('User should enter usename', function () {
    ticketPage.setUserName()
});

Then('user should enter password', function () {
     ticketPage.setPassword()
});

Then('User should click on login button', function () {
    ticketPage.setLoginBtn()
    
});

Then('User should click on event detail page',function () {
    ticketPage.setEventCard()

 });
  
Then('User enter the friends name',{timeout:60*90}, function () {

     ticketPage.setFriendName()

  });

Then('User should add the friends in list', function () {
    ticketPage.getSelectSuggestion()

  });

  Then('user should click on add button', function () {
    ticketPage.setAddButton()

  });

Then('user should click on book button', function () {
      ticketPage.setBookButton()
  });

Then('user should click on I agree button', function () {
    ticketPage.setCheckButton()
    
  });

Then('user should click on book confirm button', function () {
    ticketPage.setBookBtn()
  });