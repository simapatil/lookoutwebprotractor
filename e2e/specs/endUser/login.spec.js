let specPage = require('../../pages/endUser/login.po.js');

let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;
// var { setDefaultTimeout } = require('cucumber');
// setDefaultTimeout(60 * 1200);

Before(function () {
    specPage.init();
})

Given('The user go to {string}', function (string) {
    specPage.get(string)
});


When('User click on user icon', function () {
    specPage.setIcon()

});

Then('User should enter usename', function () {
    specPage.setUserName()
});

Then('user should enter password', function () {
     specPage.setPassword()
});

Then('User should click on login button',{timeout:60*1000}, function () {
    specPage.setLoginBtn()
    
  });

