let likePage = require('../../pages/endUser/likeComment.po.js');

let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;
// var { setDefaultTimeout } = require('cucumber');
// setDefaultTimeout(60 * 1200);

Before(function () {
    likePage.init();
})

Given('The user go to {string}', function (string) {
    likePage.get(string)
});


When('User click on user icon', function () {
    likePage.setIcon()

});

Then('User should enter usename', function () {
    likePage.setUserName()
});

Then('user should enter password', function () {
    likePage.setPassword()
});

Then('User should click on login button',{timeout:60*1000}, function () {
    likePage.setLoginBtn()
    
  });

  Then('User should click on like button', function () {
      likePage.getLike()
    
  });

  Then('User should click on Comment button', function () {
      likePage.setCommentIcon()
    
  });

  Then('User should enter the comment', function () {
    likePage.getCommentTExt()
  });

  Then('User should click send button', function () {
    likePage.setSendButton()
  });

