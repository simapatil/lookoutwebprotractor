let reportPage = require('../../pages/endUser/reportEvent.po.js')
let chai = require('chai')
let chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
let expert = chai.expect;

Before (function(){
    reportPage.init();
})

  Given('The user go to {string}', function (string) {
   reportPage.get(string)
  });


  When('User click on user icon', function () {
    reportPage.setIcon()
  });



  Then('User should enter username', function () {
    reportPage.setUserName()
  });



  Then('user should enter password', function () {
    reportPage.setPassword()
  });



  Then('User should click on login button', function () {
    reportPage.setLoginBtn()
  });



  Then('User should click on option menu', function () {
    reportPage.setOptionMenu()
  });



  Then('User should click on report option', function () {
    reportPage.getReportBtn()
  });



  Then('User should select one option', function () {
    reportPage.setSelectOption()
  });



  Then('user should click on confirm button', function () {
   reportPage.getsendReportBtn()
  });