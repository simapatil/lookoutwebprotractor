let changePasswordPage = require('../../pages/endUser/changePassword.po.js')
let chai = require('chai')
let chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
let expert = chai.expect;

Before (function(){
    changePasswordPage.init();
})

  Given('The user go to {string}', function (string) {
    changePasswordPage.get(string)
  });

  When('User click on user icon', function () {
    changePasswordPage.setIcon()
  });

  Then('User should enter username', function () {
    changePasswordPage.setUserName()
  });


  Then('user should enter password', function () {
    changePasswordPage.setPassword()
  });

  Then('User should click on login button', function () {
    changePasswordPage.setLoginBtn()
  });

  Then('User should click on user icon', function () {
    changePasswordPage.setUserIcon()
  });

  Then('User should select setting option', function () {
    changePasswordPage.getSetting()
  });

  Then('User should select the general setting', function () {
    changePasswordPage.setGeneralSetting()
  });

  Then('User should select the change password option', function () {
      changePasswordPage.setChangePassword()
   
  });
  Then('User should enter old password', function () {
     changePasswordPage.setOldPassword()
  });
  Then('User should enter new password', function () {
    changePasswordPage.setNewPassword()
    
  });
  Then('User should click on submit button', function () {
    changePasswordPage.setSubmitButton()
  });