let loginPage = require('../../pages/endUser/loginwithgoogle.po.js');

let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;
// var { setDefaultTimeout } = require('cucumber');
// setDefaultTimeout(60 * 1200);

Before(function () {
    loginPage.init();
})

        Given('The user go to {string}', function (string) {
         loginPage.get(string)
          });

         When('User click on login icon', function () {
             loginPage.setIcon()
          
         });

         Then('Then User click on google icon', function () {
           loginPage.getGoogleIcon()
         });

      
         Then('User should enter the email id', function () {
             
             loginPage.setEmailId()
          });

        Then('User should click on next Button', function () {

            loginPage.getNexButton()
          });
