let eventPost = require("./../../pages/clubUser/eventPost.po.js");
let chai = require('chai')
let chaiasPromised = require('chai-as-promised')
chai.use(chaiasPromised)
let expert = chai.expect;

Before(function(){
eventPost.init()
});

Given('The user go to {string}', function (string) {
    eventPost.get(string)
  });
When('User click on user icon', function () {
    eventPost.setIcon()
     });

  Then('User should enter usename', function () {
    eventPost.setUserName()
  });
  Then('user should enter password', function () {
    eventPost.setPassword()
  });
  Then('User should click on login button', function () {
    eventPost.setLoginBtn()
  });

  Then('User should click on Create event button', function () {
   eventPost.setCreateEvent()
  });

  Then('User should enter event name', function () {
  eventPost.setEventname()
  });
  Then('User should select the event date', function () {
   eventPost.setCreateEvent()
  });
  Then('User should select date from popup', function () {
  eventPost.setEventDate()
  });
  Then('User should enter start hh time', function () {
    eventPost.setStarthh()
  });
  Then('User should enter start mm time', function () {
    eventPost.setSartMM()
  });

  Then('User should enter end hh time', function () {
    eventPost.setEndHH()
  });

  Then('User should enter end mm time', function () {
    eventPost.setEndMM()
  });
  Then('User should enter eventDescription', function () {
    eventPost.setEventDiscription()
  });

 Then('User should click on next button', function () {
      eventPost.setNextButton()
    });
 
  Then('User should enter performingDj', function () {
        eventPost.setPerformingDj();
      });

  Then('User should enter collabrotar', function () {
        // Write code here that turns the phrase above into concrete actions
        return 'pending';
      });

  Then('User should enter tag', function () {
        // Write code here that turns the phrase above into concrete actions
        return 'pending';
      });
