let specPage = require('../../pages/clubUser/imageUpload.po.js');
let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;
Before (function(){
specPage.init();
});
Given('The user go to {string}', function (string) {
specPage.hiturl(string);
});


When('User click on login icon', function () {
specPage.loginpage();
});


Then('User enters username', function () {
specPage.enterUsername();
});


Then('User enters password', function () {
specPage.enterPassword();
});

Then('User clicks on login button', function () {
specPage.clickonloginButton();
}); 
Then('user clicks on image button', function () {
specPage.clickonImageButton();
});

Then('user clicks on upload button', function () {
specPage.clickonUploadButton();
});

// Then('user uploads an image', function () {
// specPage.uploadImage();
// });



Then('user clicks on save changes button', function () {
specPage.clickonSaveButton();
});

Then('user clicks on post button', function () {
specPage.clickonPostButton();
});
