let likePage = require('../../pages/clubUser/likeComment.po.js')
let chai = require('chai')
let chaiasPromised = require('chai-as-promised')
chai.use(chaiasPromised)
let expert = chai.expect;


Before(function(){
    likePage.init()
    });
    
 Given('The user go to {string}', function (string) {
   likePage.get(string);    
  });

  When('User click on user icon', function () {
    likePage.setIcon();
  });

  Then('User should enter usename', function () {
    likePage.setUserName();
  });

  Then('user should enter password', function () {
    likePage.setPassword();
  });

  Then('User should click on login button', function () {
    likePage.setLoginBtn();
  });

  Then('User should click on Like button', function () {
   likePage.setLikeButton();
  });

  Then('User should click on comment button', function () {
    likePage.setCommentClick();
  });
  Then('User should write comment', function () {
    likePage.setCommentText();
  });
  Then('User should click on send button', function () {
      likePage.setSendButton();
  });