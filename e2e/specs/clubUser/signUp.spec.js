let ClubSignUp = require('../../pages/clubUser/signUp.po.js')
let chai = require('chai')
let chaiasPromised = require('chai-as-promised')
chai.use(chaiasPromised)
let expert = chai.expect;


Before(function(){
    ClubSignUp.init()
});
Given('The user go to {string}', function (string) {
  ClubSignUp.get(string)
});

Then('User should click on user Icon', function () {
 ClubSignUp.setIcon()
});

Then('User should click on Dont have an account?Sign Up', function () {
  ClubSignUp.setSignUpLink()
});

Then('User should click on Signup link', function () {
  ClubSignUp.setclubUserSignUp()
});

Then('user should enter Club Name', function () {
 ClubSignUp.setClubName()
});

Then('User should enter Phone number', function () {
  ClubSignUp.setPhoneNumber()
});

Then('User should enter Email address', function () {
  ClubSignUp.setEmailId()
});

Then('User should enter Introduction', function () {
  ClubSignUp.setIntroduction()
});

Then('User should enter Username', function () {
  ClubSignUp.setUsername()
});

Then('User should enter Password', function () {
  ClubSignUp.setPassword()
});

Then('User should  click on Next button', function () {
  ClubSignUp.setNextButton()
});

Then('User should select Location', function () {
 ClubSignUp.setClubLocation()
});

Then('User should enter Locaton Name', function () {
  ClubSignUp.setLocationName()
});

Then('User should select suggested option', function () {
  ClubSignUp.setSuggestion()
});


Then('User Should click on SaveChange button', function () {
  ClubSignUp.setSaveChangeButton()
});


Then('User should enter State', function () {
 ClubSignUp.setState()
});

Then('User should select State from Suggestion', function () {
  ClubSignUp.setStateSuggestion()
});

Then('User should enter City', function () {
  ClubSignUp.setCity()
});

Then('User should select City from suggestion', function () {
  ClubSignUp.setCitySuggestion()
});

Then('User should enter Street', function () {
  ClubSignUp.setStreet()
  
});

Then('User should enter ZipCode', function () {
  ClubSignUp.setZipCode()
});

Then('User should enter From hour time', function () {
   ClubSignUp.setFromHour()
  });

Then('User should enter From minutes time', function () {
    ClubSignUp.setFromMinutes()
  });

Then('User should enter To hour time', function () {
    ClubSignUp.setToHour()
  });
 Then('User should enter To minutes time', function () {
    ClubSignUp.setToMinutes()
  });

  Then('User should select day', function () {
    ClubSignUp.setClickDay()
  });

  Then('User should Select day from options', function () {
   ClubSignUp.setSelectDay()
  });

  Then('User should click on out side of the dropdown', function () {
   ClubSignUp.setOutSideClick( )
  });
  Then('User should click on signUp', function () {
    ClubSignUp.setRequestButton()
  });

