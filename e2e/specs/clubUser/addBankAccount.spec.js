let BankAccount = require("../../pages/clubUser/addBankAccount.po.js");
let chai = require('chai')
let chaiasPromised = require('chai-as-promised')
chai.use(chaiasPromised)
let expert = chai.expect;

Before(function(){
  BankAccount.init()
});

         Given('The user go to {string}', function (string) {
           BankAccount.get(string)
         });

         When('User click on user icon', function () {
          BankAccount.setIcon()
         });
         
         Then('User should enter usename', function () {
          BankAccount.setUserName()
         });

         Then('user should enter password', function () {
          BankAccount.setPassword()

         });

         Then('User should click on login button', function () {
          BankAccount.setLoginBtn()

         });

         Then('User click on Loginuser icon', function () {
          BankAccount.setUserIcon()

        });
         
         Then('Click on setting option', function () {
           BankAccount.setSetting()
         });

         Then('Click on Bank setting', function () {
           BankAccount.setBankSettings()
         });

         Then('Click  on add bank account', function () {
           BankAccount.setAddBankAcc()
         });
         Then('Enter IFSC code', function () {
          BankAccount.setIFSC()
        });

        Then('Enter Account number', function () {
          BankAccount.setAccountNo()
        });

        Then('enter account number for confirmation', function () {
          BankAccount.setConfirmAccNo()
        });

        Then('enter account holder name', function () {
          BankAccount.setAccHolderName()
        });

        Then('click on  the account type option', function () {
          BankAccount.SetAccType()
        });

        Then('click on Saving Account  option', function () {
          BankAccount.setCurrrenAcc()
        });

        Then('enter the nick name', function () {
          BankAccount.setNickName()
        });

        Then('click on Add Account button', function () {
          BankAccount.setAddAccount()
        });